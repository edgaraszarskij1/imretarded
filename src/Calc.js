import React, { useState, useReducer } from 'react';
import './Calculator.css'

function Calculator(props) {

  const memoryCount = (prevOperation, val1, val2) => {
    if (prevOperation === '+') {
      return val1 + val2
    }
    if (prevOperation === '-') {
      return val1 - val2
    }
    if (prevOperation === '*') {
      return val1 * val2
    }
    if (prevOperation === '/') {
      return val1 / val2
    }
  }

  const [state, dispatch] = useReducer(
    (state, action) => {
      let numLength = 0;
      numLength = state.num.toString().length
      switch (action.type) {
        case 'NUM':
          if (numLength < 8) {
            if (state.changed === true) {
              return { ...state, num: state.num * 10 + action.value, changed: action.changed };
            } else return { ...state, count: 0, displayCount: '', num: state.oldNum * 10 + action.value, changed: action.changed };
          } else return { ...state }
        case 'PURGE':
          return { ...state, num: 0, count: 0, displayCount: '', memory: '+', oldNum: 0, clicked: 0, changed: false };
        case 'DELETE':
          return { ...state, num: Math.trunc(state.num / 10) };
        case 'OPERATION':
          const operationResult = memoryCount(state.memory, state.count, state.num)
          return { ...state, count: operationResult, memory: action.value, num: 0, displayCount: state.displayCount + state.num + action.value }
        case 'RESULT':
          const result = memoryCount(state.memory, state.count, state.num)
          console.log("from action: ", state.count)
          if (state.displayCount.includes('=')) {
            return { ...state, count: result, memory: state.memory, num: state.num, changed: action.changed, displayCount: state.displayCount.substring(0, state.displayCount.lastIndexOf('=') + 1) + ' ', clicked: state.clicked + 1 }
          } else return { ...state, count: result, memory: state.memory, num: state.num, changed: action.changed, displayCount: state.displayCount + state.num + ' = ' + +(state.count + +state.num), clicked: state.clicked + 1 }
        default:
          throw new Error();
      }
    },
    {
      memory: '+',
      count: 0,
      displayCount: '',
      num: 0,
      oldNum: 0,
      changed: false,
      clicked: 0
    }
  )

  const isNumber = (variable) => {


  }
  const handleKeyPress = (event) => {

    const regex = /^[0-9]+$/g;
    let parsedVariable = parseInt(event.key)

    if (event.key.match(regex)) {
      dispatch({ type: 'NUM', value: parsedVariable, changed: true })
    }
    if (event.key === '*') {
      dispatch({ type: 'OPERATION', value: '*', changed: true })
    }
    if (event.key === '/') {
      dispatch({ type: 'OPERATION', value: '/', changed: true })
    }
    if (event.key === '+') {
      dispatch({ type: 'OPERATION', value: '+', changed: true })
    }
    if (event.key === '-') {
      dispatch({ type: 'OPERATION', value: '-', changed: true })
    }
    if (event.key === '=') {
      dispatch({ type: 'RESULT', changed: false })
    }
    
  }


  return (

    <div className={'container'} tabIndex="0" onKeyPress={handleKeyPress}>
      <div className={'screen'}>

        <p>
          display{state.displayCount}
        </p>
        <p>
          sum   {state.num === 0 || state.clicked > 1 ? state.count : state.num}
        </p>

      </div>
      <div className={'top'}>
        <button className={'top-button'} onClick={() => dispatch({ type: 'OPERATION', value: '-' })} >-</button>
        <button className={'top-button'} onClick={() => dispatch({ type: 'OPERATION', value: '*' })}>*</button>
        <button className={'top-button'} onClick={() => dispatch({ type: 'OPERATION', value: '/' })}>/</button>
        <button className={'top-button'} onClick={() => dispatch({ type: 'PURGE' })}>CA</button>
      </div>

      <div className={'side'}>
        <button className={'big-button'} onClick={() => dispatch({ type: 'OPERATION', value: '+' })}>+</button>
        <button className={'big-button'} onClick={() => dispatch({ type: 'RESULT', changed: false })}>=</button>
      </div>

      <div className={'box'}>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 7, changed: true })}>7</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 8, changed: true })}>8</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 9, changed: true })}>9</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 4, changed: true })}>4</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 5, changed: true })}>5</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 6, changed: true })}>6</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 1, changed: true })}>1</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 2, changed: true })}>2</button>
        <button className={'button'} onClick={() => dispatch({ type: 'NUM', value: 3, changed: true })}>3</button>
      </div>

      <div className={'bottom'}>
        <button className={'zero-button'} onClick={() => dispatch({ type: 'NUM', value: 0, changed: true })}>0</button>
        <button className={'button'} onClick={() => dispatch({ type: 'DELETE' })}>C</button>
      </div>
    </div>

  )
}

export default Calculator;