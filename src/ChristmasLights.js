import React, { useRef, useEffect, useState, createRef } from 'react';
import './christmasLights.css'

function ChristamsLights(props) {
	const [turnOn, setTurnOn] = useState(false)
	const [animationDuration, setAnimationDuration] = useState('')
	const [elementsRefs, setElementsRefs] = useState([])

	const lightsArray = []

	for (let i = 0; i < 7; i++) {
		lightsArray.push(i)
	}

	const lightsArrayLength = lightsArray.length

	useEffect(() => {
		setElementsRefs(elementsRefs => (
			Array(lightsArrayLength).fill().map((_, index) => elementsRefs[index] || createRef())
		))
	}, [lightsArrayLength])

	const onChangeHangler = (event) => {
		setAnimationDuration(event.target.value)
	}

	const setTimer = () => {
		if (turnOn) {
			elementsRefs.map((element, index) => {
				if (index % 2 === 0) {
					return [element.current.style.animationDuration = animationDuration, element.current.style.animationDelay = animationDuration]
				} else
					return element.current.style.animationDuration = animationDuration
			}
			)
			setAnimationDuration(elementsRefs[0].current.style.animationDuration)
		}
	}

	return (
		<div className={'container'}>{lightsArray.map((light, index) => {
			return turnOn ? <div ref={elementsRefs[index]} key={light} className='circle' > </div> : <div key={light} className='no-circle' > </div>
		})}
			<input onChange={onChangeHangler} ></input>
			<button className={'button'} onClick={() => setTurnOn(prevState => !prevState)}>{turnOn ? 'Turn OFF' : 'Turn ON'}</button>
			<button onClick={setTimer}>Set new time</button>
		</div>
	)
}

export default ChristamsLights