import React, { useReducer, useState, useEffect } from 'react'
import './colorCycle.css'

function ColorCycle(props) {

  const [colorsArr, setColorsArr] = useState([])
  const [start, setStart] = useState(false)
  const [fillColor, setFillCOlor] = useState({ backgroundColor: '#ffffff' })
  const [intervalRef, setIntervalRef] = useState()
  const [newStr, setNewStr] = useState('')
  const [color, setColor] = useState(
    {
      red: 'ff',
      blue: 'ff',
      green: 'ff'
    }
  )

  const [increment, setIncrement] = useReducer(
    (state, newState) => ({ ...state, ...newState }), {
    redInc: 0,
    blueInc: 0,
    greenInc: 0
  }
  )

  const onChangeHandler = (event) => {
    const regex = /^[0-9a-fA-F]/g;
    if(!start){
    if (event.target.value.length > 2) {
      event.target.value = event.target.value.substr(0, 2)
      setColor({ ...color, [event.target.name]: event.target.value })
    }
    if (event.target.value.length === 0) {
      setColor({ ...color, [event.target.name]: '' })
    }
    if (!event.target.value.match(regex)) {
      setNewStr(event.target.value.slice(0, event.target.value.length - 1))
      event.target.value = newStr
    }
    setColor({ ...color, [event.target.name]: event.target.value })
  }
  }

  const onIncrement = (event) => {
    const maxValue = 255
    const regex = /^[0-9]/g;
    let toInt = parseInt(event.target.value)
    if(!start){
    if (event.target.value === "" || (event.target.value.match(regex) && event.target.value <= maxValue)) {
      if (isNaN(toInt)) {
        setIncrement({ [event.target.name]: '' })
      } else
        setIncrement({ [event.target.name]: toInt })
    }
  }
  }

  const checkColor = (inc, color) => {
    let newNum = 0
    const maxValue = 255
    let newHex = 0
    setColor(prevColor => {
      if (newNum + inc <= maxValue) {
        newNum = parseInt(prevColor[color], 16) + inc
        newHex = newNum.toString(16)
        return { ...prevColor, [color]: newHex }
      } else return { ...prevColor }
    }
    )
  }

  const addNumber = (incrementBy, col) => {
    if (col === 'red') {
      checkColor(incrementBy, 'red')
    }
    if (col === 'blue') {
      checkColor(incrementBy, 'blue')
    }
    if (col === 'green') {
      checkColor(incrementBy, 'green')
    }
  }

  useEffect(() => {
    if (start && colorsArr.includes('#' + color.red + color.blue + color.green) === false) {
      setColorsArr(prevColors => ([...prevColors, '#' + color.red + color.blue + color.green]))
    }
  }, [color, colorsArr, start])

  const onIncrementClick = () => (
   [ addNumber(increment.redInc, 'red'),
    addNumber(increment.blueInc, 'blue'),
    addNumber(increment.greenInc, 'green')]
  )

  useEffect(() => {
    for (let i = 0; i <= colorsArr.length; i++) {
      if (colorsArr.length !== 0) {
        setTimeout(function (colorsArr) {
          if (colorsArr !== undefined) {
            setFillCOlor({ backgroundColor: colorsArr })
          }
        }, i * 250, colorsArr[i], i)
      }
    }
  }, [colorsArr])

  return (
    <div>
      red <input name={'red'} onChange={onChangeHandler} value={color.red} />
      <input name={'redInc'} onChange={onIncrement} value={increment.redInc} />

      blue <input name={'blue'} onChange={onChangeHandler} value={color.blue} />
      <input name={'blueInc'} onChange={onIncrement} value={increment.blueInc} />

      green <input name={'green'} onChange={onChangeHandler} value={color.green} />
      <input name={'greenInc'} onChange={onIncrement} value={increment.greenInc} />
      <input readOnly value={'#' + color.red + color.blue + color.green} />
      {!start ? 
      <button onClick={() => { setIntervalRef(setInterval(onIncrementClick, 250)); setStart(true) }}>Start</button>
      :<button onClick={() => { clearInterval(intervalRef); setStart(false) }}>Stop</button>        
      }
      <p className={'box'} style={fillColor} >{fillColor.backgroundColor}</p>
    </div>
  )

}

export default ColorCycle;