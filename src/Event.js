class Event{
    registeredFunctions = [];

    register(func) {
      this.registeredFunctions.push(func);
    }
  
    unregister(func) {
      this.registeredFunctions.splice(this.registeredFunctions.indexOf(func), 1);
    }
  
    notify() {
      for (const func of this.registeredFunctions) {
        func();
      }
    }
}

export default Event