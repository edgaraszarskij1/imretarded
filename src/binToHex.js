
function binToDec(num){
    let intToString = num.toString()
    const regex = /^[0-1]+$/g;
    let sum = 0;
    let powBy = intToString.length-1
    if(intToString.match(regex)){
        for(let i = 0; i < intToString.length; i++){
            sum += intToString[i]*Math.pow(2,powBy--)
        }
        return sum
    } else alert('Containing invalid numbers or letters')
}

function binToDec2(num){
    let intToString = num.toString()
    let sum = 0;
    const regex = /^[0-1]+$/g;
    if(intToString.match(regex)){
        for(let i = 0; i < intToString.length; i++){
            sum = (sum*2)+ +intToString[i]
        }
        return sum
    } else alert('Containing invalid numbers or letters')
}