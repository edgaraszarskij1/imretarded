import React, {useState,useReducer,useRef} from 'react';
import './invisButton.css'

function BorderTask(props){
const [border,setBorder] = useReducer(
    (state,newState) => ({...state,...newState}),{
        borderRa:`10px 10px 10px 5px`,
        back:'red',
        width:'200px',
        height:'200px'
    }
)

//const [test,setTest] = useState([`10px 10px 10px 5px`,'red','200px','200px'])

let valuesArray = Object.values(border); 
  
for (let value of valuesArray) { 
  console.log(value); 
} 


const borderRef = useRef(null);
const backgroundRef = useRef(null);
const widthRef = useRef(null);
const heightRef = useRef(null);
const invisRef = useRef(null)

const handleChange= (event)=>{
    console.log(event.target.value)
    setBorder({[event.target.name]:event.target.value})
}

const handleCopy = (e) => {
    invisRef.current.value = borderRef.current.value + ', ' + backgroundRef.current.value + ', ' + widthRef.current.value + ', ' + heightRef.current.value + ','
    invisRef.current.select() 
    document.execCommand('copy');

    e.target.focus();
   
}

return(<div>
    <div style={{borderRadius:border.borderRa, background:border.back, width:border.width,height:border.height, margin:'auto'}}>asd</div>
    {/* <textarea name={['back'|| 'borderRa']} value={[border.back || border.borderRa]} onChange={handleChange}/> */}
    <input name={'borderRa'} value={border.borderRa} onChange={handleChange} ref={borderRef} />
    <input name={'back'} value={border.back } onChange={handleChange} ref={backgroundRef}/>
    <input name={'width'} value={border.width } onChange={handleChange} ref={widthRef}/>
    <input name={'height'} value={border.height} onChange={handleChange} ref={heightRef}/>
    <input className={'txt-invisible'} ref={invisRef}/>
    <button onClick={(e) => handleCopy(e)}>Copy</button>
</div>)
}

export default BorderTask