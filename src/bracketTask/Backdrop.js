import React from 'react'
import './bracket.css'

const Backdrop = (props) => {
    return(
        <>
        {props.isOpen? <div data-testid="1" className='shadow'></div> : <div></div>}
        </>
    )
}

export default Backdrop;