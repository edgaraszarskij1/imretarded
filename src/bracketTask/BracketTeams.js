import React, { useState, useEffect } from 'react'
import DisplayBracket from './DisplayBracket';
import Dialog from './Dialog'

function BracketTeams(props) {

    const [inputState, setInputState] = useState([{
        isOpen: false,
        name: "",
        index: null,
        arrayOfFields: [],
        displayBracket: false,
    }])

    const onChangeHandler = (event, index, fieldIndex) => {
        let newState = []
        const eventTargetName = event.target.name
        const eventTargetValue = event.target.value
        setInputState(oldState => {
            newState = [...oldState]
            newState[index].arrayOfFields[fieldIndex] = { [eventTargetName]: eventTargetValue }
            return newState
        })
    }

    useEffect(() => {
        let newState = []
        props.bracketTeams.map((team, index) => (
            setInputState(oldState => {
                newState = [...oldState]
                newState[index] = { ...newState[index], isOpen: !props.isOpen, name: team.tournamentName, index: index, arrayOfFields: [] }
                return newState
            })
        ))
    }, [props.bracketTeams, props.isOpen])
console.log(inputState)
    const onAssingTeamNames = (numberOfTeams, tournamentName, index) => {
        const newArray = []
        for (let i = 0; i < numberOfTeams; i++) {
            newArray.push(<input className={'input '} key={tournamentName + i} name={i % 2 === 0 ? 'team0' : 'team1'} defaultValue={`Enter Team ${i + 1} Name`} onChange={(event) => onChangeHandler(event, index, i)} />)
        }
        return <Dialog><span className={'title'}>Team Names:</span> <form onSubmit={(event) => onSubmit(event, index)}>{newArray} <input type="submit" value="Submit" className={'dialog-sticky-button'} />
        </form><button className={'sticky-cancel-button'} onClick={() => props.onCloseClickHandler(index)}> Cancel </button></Dialog>
    }

    const onDisplayBracketHandler = (event, index, teams) => {
        let newState = []
        event.stopPropagation()
        setInputState(oldState => {
            newState = [...oldState];
            newState[index] = { ...newState[index], displayBracket: !oldState[index].displayBracket, }
            return newState
        })
        props.onShowBracket(teams, index)
    }

    const onSubmit = (event, index) => {
        props.onDataGathered({ inputState: inputState[index].arrayOfFields, index: index });
        event.preventDefault();
        props.onCloseClickHandler(index)
    };

    return (
        <div data-testid="BracketTeams-div" >
            {props.bracketTeams.map((team, index) => (
                <div key={team.tournamentName}  data-testid="TeamsRender-div"  >
                    <div className={'tournament'} data-testid="tournament-div"  onClick={(event) => { props.onOpenClickHandler(index) }}> {team.tournamentName}</div>
                    {props.isOpen[index] !== undefined && props.isOpen[index].isOpen ?
                        onAssingTeamNames(team.competingTeamsNumber, team.tournamentName, index) : null}
                    {inputState[index] !== undefined && team.tournamentTeams.length > 0 && <div className={'show-bracket'} onClick={(event) => onDisplayBracketHandler(event, index, team.tournamentTeams)} > Show Brackets</div>}
                    {inputState[index] !== undefined && inputState[index].displayBracket &&
                        <DisplayBracket
                            index={index}
                            test={team.test}
                            teamName={team.tournamentName}
                            onChangePointsHandler={props.onChangePointsHandler}
                            onSubmitPoints={props.onSubmitPoints}
                            onDateAssign={props.onDateAssign}
                            onOpenSetDate={props.onOpenSetDate}
                            onOpenClickHandler={props.onOpenClickHandler}
                            setIsOpen={props.setIsOpen}
                            isOpen={props.isOpen}
                            onCloseClickHandler={props.onCloseClickHandler} 
                            fullBracketLength = {team.competingTeamsNumber-1}/>
                    }
                </ div>
            ))}
        </div>
    )
}

export default BracketTeams;