/** 
 * @typedef {Object} Team0
 * @property {string} team0
 */

/** 
 * @typedef {Object} Team1
 * @property {string} team1
 */

/** 
 * @typedef {Object} MatchTemplate
 * @property {number} teamOneScore
 * @property {number} teamTwoScore
 * @property {string} date
 * @property {number} column
 * @property {number} row1
 * @property {number} row2
 */

/** 
 * @typedef {Team0 & Team1 & MatchTemplate} Match
 */

/**
 * @param {Array<Team0 | Team1>} teams
 * @param {MatchTemplate} defaultMatch
 * @returns {Match}
 */
export const createMatches = (teams, defaultMatch) => {
    /** @type Array<Team0 | Team1> */
    const clonedTeams = JSON.parse(JSON.stringify(teams));

    /** @type Array<[Team0, Team1]> */
    if(teams.length>0){
    const teamPairs = [];
    for (let i = 0; i < clonedTeams.length; i += 2) {
        teamPairs.push(clonedTeams.slice(i, i + 2));
    }

    /** @type Array<Match> */
    const matches = [];
    for (let i = 0; i < teamPairs.length; i++) {
        matches.push(Object.assign(teamPairs[i][0], teamPairs[i][1], defaultMatch));
    }
    return matches; 
} else return []
 
}
