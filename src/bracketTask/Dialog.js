import React from 'react';
import Backdrop from './Backdrop';

const Dialog = (props) => {
    return (
        <div data-testid="outter-div-dialog">
              <Backdrop data-testid="backdrop" isOpen = {true}/>
            <div data-testid="inner-div-dialog" className={'dialog'}>
                {props.children}
            </div> 
        </div>

    )
}

export default Dialog