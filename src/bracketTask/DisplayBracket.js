import React, { useEffect } from 'react'
import './bracket.css'
import MatchForm from './MatchForm'

function DisplayBracket(props) {
    let countRowBy3 = 0;
    let countRowBy1 = 0;
    const { test, setIsOpen } = props
    const gridItemHeight = 40
    const lineHeightPositionUp = 7
    const lineHeightPositionDown = 5
    const halfOfSize = 2
    const firstThirdRow = 3
    const nextThirdRow = 4
    const firstRow = 1

    useEffect(() => {
        let newState = []
        test.map((name, index) => (
            setIsOpen(oldState => {
                newState = [...oldState]
                newState[index] = { isOpen: false, isSetScoreOpen: false }
                return newState
            })
        ))
    }, [test, setIsOpen])

    const onChangeHandler = (event, index, ind) => {
        props.onDateAssign(index, ind, event.target.value)
    }

    const onCreateLines = (index, column) => {
        let collection = []

        if (index % 2 === 0) {
            collection.push(<div className={'line-down'} style={{ height: gridItemHeight / halfOfSize * (Math.pow(2, column) - 1), bottom: gridItemHeight / halfOfSize - lineHeightPositionUp }}></div>)
        }
        if (index % 2 !== 0) {
            collection.push(<div className={'line-up'} style={{ height: gridItemHeight / halfOfSize * (Math.pow(2, column) - 1), bottom: gridItemHeight / 2 * (Math.pow(2, column) - 1) + gridItemHeight / halfOfSize - lineHeightPositionDown }}></div>)
        }
        return <div>{collection}</div>
    }

    const bracketLayout = (team, index) => {
        if (team.win) {
            return {
                gridRow: countRowBy1 = (team.row1 + team.row2) / 2,
                gridColumn: team.column, countRowBy3: countRowBy3 = (team.row1 + team.row2) / 2
            }
        }
        if (index === 0) {
            return {
                gridRow: countRowBy1 += firstRow
            }
        }
        if (index === 1) {
            return {
                gridRow: countRowBy3 += firstThirdRow
            }
        }
        if (index > 1 && index % 2 === 0) {
            return {
                gridRow: countRowBy1 += nextThirdRow
            }
        } else {
            return {
                gridRow: countRowBy3 += nextThirdRow
            }
        }
    }

    return (
        <div className={'grid-container'} style={{ gridAutoRows: gridItemHeight }} data-testid="DisplayBracket-div" > 
            {test.map((team, index) =>
                <div key={index} className={'grid-item'}
                    style={bracketLayout(team, index)}  >
                    <div onClick={() => props.onOpenClickHandler(index)}>

                        {team.team0 + ' '}
                        {team.teamOneScore + ' '}

                        <div className={'hr'} />
                        {team.team1 + ' '}
                        {team.teamTwoScore + ' '}

                    </div>

                    {/* {
                        props.isOpen[index] !== undefined && props.isOpen[index].isOpen &&
                        <input name='matchDate' type="date" required onChange={(event) => onChangeHandler(event, props.index, index)} />
                    } */}
                    {props.isOpen[index] !== undefined && props.isOpen[index].isOpen &&
                        <MatchForm
                            index={props.index}
                            fieldIndex={index}
                            currIndex={props.test.length - 1}
                            countRowBy1={countRowBy1}
                            countRowBy3={countRowBy3}
                            col={team.column} team={team}
                            onChangePointsHandler={props.onChangePointsHandler}
                            onSubmitPoints={props.onSubmitPoints}
                            onCloseClickHandler={props.onCloseClickHandler}
                            onOpenClickHandler={props.onOpenClickHandler}
                        />

                    }
                    <span className={'shit'}></span>
                    <div>{props.fullBracketLength === index + 1 ? null : onCreateLines(index, team.column)}</div>
                </div>
            )}
        </div>
    )
}

export default DisplayBracket