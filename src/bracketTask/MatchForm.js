import React from 'react'
import Dialog from './Dialog'
const MatchForm = (props) => {

    return (
        <Dialog  >
            <span className={'title'}>Match Score:</span>
            <form  data-testid="MatchForm-div" onSubmit={(event) => { props.onSubmitPoints(event, props.index, props.fieldIndex, props.currIndex, props.countRowBy1, props.countRowBy3, props.col); props.onOpenClickHandler(props.fieldIndex) }}>
                <div className={'team-span'}>Team:{props.team.team0}</div>
                <input name='teamOneScore' data-testid="filter-input-teamOneScore" onChange={(event) => props.onChangePointsHandler(event, props.index, props.fieldIndex)} defaultValue={props.team.teamOneScore} className={'input '} />
                <div className={'team-span'}>Team:{props.team.team1} </div>
                <input name='teamTwoScore' data-testid="filter-input-teamTwoScore" defaultValue={props.team.teamTwoScore} onChange={(event) => props.onChangePointsHandler(event, props.index, props.fieldIndex)} className={'input '} />
                <input type='submit' value='Submit' className={'dialog-button'} />
                <button className={'cancel-button'} onClick={() => props.onCloseClickHandler(props.fieldIndex)}>Cancel</button>
            </form>
        </Dialog>
    )
}

export default MatchForm