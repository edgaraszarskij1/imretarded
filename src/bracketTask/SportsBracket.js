import React, { useState, useReducer } from 'react';
import BracketTeams from './BracketTeams';
import './bracket.css'
import {createMatches} from './CreateMatches'
import match from './upperBrackets'
//import shuffle from './shuffle';

const initialState = {
    tournamentName: '',
    startingDate: '',
    endingDate: '',
    competingTeamsNumber: '',
    error: false,
    message: '',
    createdAt: new Date().toJSON().slice(0, 10),
    tournamentTeams: [],
    test: [{
        teamOneScore: 0,
        teamTwoScore: 0,
        date: '',
        column: 1,
        row1: 1,
        row2: 0
    }],
    shuffled: false,
}


const reducer = (state, action) => {
    switch (action.type) {
        case 'FORM':
            switch (action.field) {
                case 'tournamentName':
                    return { ...state, tournamentName: action.value }
                case 'startingDate':
                    if (action.value < state.createdAt) {
                        return { ...state, error: true, message: "Start date can't be set before current day" }
                    } else return { ...state, error: false, startingDate: action.value }
                case 'endingDate':
                    if (action.value < state.createdAt) {
                        return { ...state, error: true, message: "End date can't be set before current day" }
                    }
                    if (action.value < state.startingDate) {
                        return { ...state, error: true, message: "End date can't be set before start day" }
                    } else return { ...state, error: false, endingDate: action.value }
                case 'competingTeamsNumber':
                    if (action.value % 2 !== 0) {
                        return { ...state, error: true, message: 'Odd number of teams' }
                    } else return { ...state, competingTeamsNumber: action.value, error: false, }
                default:
                    throw new Error()
            }
        default:
            throw new Error()
    }
}

function SportsBracket() {
    const [state, dispatch] = useReducer(reducer, initialState)
    const [tournament, setTournament] = useState([])
    const [isOpen, setIsOpen] = useState([{ isOpen: false, isSetScoreOpen: false }])

    const onChangeHandler = (event) => {
        dispatch({ type: 'FORM', field: event.target.name, value: event.target.value })
    }

    const handleSubmit = (event) => {
        if (!state.error) {
            setTournament(prevBracket => [...prevBracket,
            {
                createdAt: state.createdAt,
                tournamentName: state.tournamentName,
                startingDate: state.startingDate,
                endingDate: state.endingDate,
                competingTeamsNumber: state.competingTeamsNumber,
                tournamentTeams: state.tournamentTeams
            }]
            )
        }
        event.preventDefault();
    }
  console.log(tournament)

    const onDataGathered = data => {
        if (!state.error) {
            let newState = []
            setTournament(oldState => {
                newState = [...oldState];
                newState[data.index] = { ...newState[data.index], tournamentTeams: data.inputState }
                return newState
            }
            )
        }
    }

    const showBracket = (array, index) => {
        let newState = 0;
        setTournament(oldState => {
            newState = [...oldState];
            newState[index] = { ...newState[index], test: createMatches(array, state.test[0]) }
            return newState
        }
        )
    }

    const onDateAssign = (index, fieldIndex, setDate) => {
        let newState = []
        setTournament(oldState => {
            newState = [...oldState]
            newState[index].test[fieldIndex] = { ...newState[index].test[fieldIndex], date: setDate }
            return newState
        })
    }

    const onChangePointsHandler = (event, index, fieldIndex) => {
        let newState = []
        const eventTargetName = event.target.name
        const eventTargetValue = event.target.value
        setTournament(oldState => {
            newState = [...oldState]
            newState[index].test[fieldIndex] = { ...newState[index].test[fieldIndex], [eventTargetName]: eventTargetValue }
            return newState
        })
    }

    const onSubmitPoints = (event, index, fieldIndex, currId, currRow, currRow2, column) => {
        event.preventDefault();
        let newState = []
        let oldTest = []
        setTournament(oldState => {

            newState = [...oldState]
            if (fieldIndex % 2 === 0) {
                if (newState[index].test[fieldIndex].teamOneScore > newState[index].test[fieldIndex].teamTwoScore) {
                    match.firstColumnFirstTeamWin(index, fieldIndex, currRow, currRow2, column, oldState, newState)
                    console.log(index, fieldIndex, currRow, currRow2, column, oldState, newState)
                } else {
                    match.firstColumnSecondTeamWin(index, fieldIndex, currRow, column, oldTest, newState)
                }
            } else {
                if (newState[index].test[fieldIndex].teamOneScore > newState[index].test[fieldIndex].teamTwoScore) {
                    match.secondColumnFirstTeamWin(index, fieldIndex, currId, currRow2, oldTest, newState)
                } else {
                    match.secondColumnSecondTeamWin(index, fieldIndex,currId, currRow2,oldTest,newState)
                }
            }
            return newState
        })
    }

    const onOpenClickHandler = (index) => {
        let newState = []
        console.log(index)
        setIsOpen(oldState => {
            newState = [...oldState];
            newState[index] = { ...newState[index], isOpen: true, index: index }
            return newState
        })
    }

    const onCloseClickHandler = (index) => {
        let newState = []
        setIsOpen(oldState => {
            newState = [...oldState];
            newState[index] = { ...newState[index], isOpen: false, index: index }
            return newState
        })
    }

    return (
        <div data-testid="SportsBracket-div" >
            <form onSubmit={handleSubmit} className={'center'} >
                Tournament Name
                <input name='tournamentName' data-testid="filter-input-name" required onChange={onChangeHandler} className={'input'} />
                Starting Date
                <input name='startingDate' data-testid="filter-input-startingDate" type="date" required onChange={onChangeHandler} className={'input'} />
                Ending Date
                <input name='endingDate' data-testid="filter-input-endingDate" type="date" required onChange={onChangeHandler} className={'input'} />
                Participating Teams Number
                <input name='competingTeamsNumber' data-testid="filter-input-teamNumber" required onChange={onChangeHandler} className={'input'} />
                <input type="submit" value="Submit" className={'button-tournament'} />
                {state.error && <p style={{ color: 'red' }}>{state.message}</p>}
            </form>

            <BracketTeams
                bracketTeams={tournament}
                onDataGathered={onDataGathered}
                onShowBracket={showBracket}
                onChangePointsHandler={onChangePointsHandler}
                onSubmitPoints={onSubmitPoints}
                onDateAssign={onDateAssign}
                onOpenClickHandler={onOpenClickHandler}
                onCloseClickHandler={onCloseClickHandler}
                setIsOpen={setIsOpen}
                isOpen={isOpen}
            />
        </div>
    )
}

export default SportsBracket;