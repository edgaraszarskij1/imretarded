import React from 'react';
import Backdrop from '../Backdrop';
import {render} from '@testing-library/react';


it("Checks if passed value is false and returns null", () =>{
    const {queryByTestId} = render(
        <Backdrop  isOpen = {false}  />,
    )
        expect(queryByTestId('1')).toBeFalsy()
})

it("Checks if passed value is true and returns it return div", () =>{
    const {queryByTestId} = render(
        <Backdrop isOpen = {true} />,
    )
        expect(queryByTestId('1')).toBeTruthy()
    })
