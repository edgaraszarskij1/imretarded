import React from 'react';
import BracketTeams from '../BracketTeams';
import { render, fireEvent } from '@testing-library/react';

describe('BracketTeams component', () => {
    const mockFunc = jest.fn()
    const bracketTeams = [{
        tournamentTeams: [],
        competingTeamsNumber:4,
        tournamentName:'asd'
    }]
    const isOpen = true
    
    it('Checks if component is loaded', () => {
        const { queryByTestId } = render(
            <BracketTeams bracketTeams={bracketTeams} isOpen={isOpen} />
        )
        expect(queryByTestId('BracketTeams-div')).toBeTruthy()
    })

    it('Checks if map items is loaded', () => {
        const { queryByTestId } = render(
            <BracketTeams bracketTeams={bracketTeams} isOpen={isOpen} />
        )
        expect(queryByTestId('TeamsRender-div')).toBeTruthy()
    })

    it('Checks if onOpen click handler is getting called', () => {
        const { getByTestId } = render(
            <BracketTeams bracketTeams={bracketTeams} isOpen={false} onOpenClickHandler={mockFunc} />
        )
        fireEvent.click(getByTestId("tournament-div"))
        expect(mockFunc).toHaveBeenCalled()
        expect(getByTestId('tournament-div')).toBeTruthy()
    })

})