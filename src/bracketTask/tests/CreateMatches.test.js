const {createMatches} = require('../CreateMatches');

test('Returns empty array when empty team array is provided', () =>{
    const dummyDefaultMatch = {
        teamOneScore: 0,
        teamTwoScore: 0,
        date: "",
        column: 1,
        row1: 1,
        row2: 0
    }
    const dummyTeams = []

    const expectedMatch = []
    const matches = createMatches(dummyTeams,dummyDefaultMatch)

    expect(matches).toEqual(expectedMatch)
})


test('Returns expected matches', () => {
    const dummyDefaultMatch = {
        teamOneScore: 0,
        teamTwoScore: 0,
        date: "",
        column: 1,
        row1: 1,
        row2: 0
    }
    const dummyTeams = [{
        team0:"1",
        team1:"2"
    }]
    const expectedMatch = [{
        column: 1,
        date: "",
        row1: 1,
        row2: 0,
        team0:"1",
        team1:"2",
        teamOneScore: 0,
        teamTwoScore: 0,
       
    }]

    const matches = createMatches(dummyTeams,dummyDefaultMatch)

    expect(matches).toEqual(expectedMatch)
});