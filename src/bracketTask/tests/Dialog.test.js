import React from 'react';
import Dialog from '../Dialog';
import { render } from '@testing-library/react';


it("Checks if component main div is loaded", () => {
    const { queryByTestId } = render(
        <Dialog />,
    )
    expect(queryByTestId('outter-div-dialog')).toBeTruthy()
})

it("Checks if backdrop is loaded", () => {
    const { queryByTestId } = render(
        <Dialog />,
    )
    expect(queryByTestId('1')).toBeTruthy()
})

it("Checks if dialog div is loaded", () => {
    const { queryByTestId } = render(
        <Dialog />,
    )
    expect(queryByTestId('inner-div-dialog')).toBeTruthy()
})



