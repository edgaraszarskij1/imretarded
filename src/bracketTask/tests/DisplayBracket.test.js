import React from 'react';
import DisplayBracket from '../DisplayBracket';
import { render, fireEvent } from '@testing-library/react';

describe('DisplayBracket component', () => {
    const mockFunc = jest.fn()
    const team = [{
        team0: 'team0',
        team1: 'team1',
        teamOneScore: 0,
        teamTwoScore: 0,
    }]
    it('Checks if DisplayBracket is loaded', () => {
        const { queryByTestId } = render(
            <DisplayBracket test={team} isOpen={false} setIsOpen={mockFunc} />
        )
        expect(queryByTestId('DisplayBracket-div')).toBeTruthy()
    })

    it('Checks if setIsOpen getting called', () => {
        const { getByTestId } = render(
            <DisplayBracket test={team} isOpen={false} setIsOpen={mockFunc} />
        )

        fireEvent.click(getByTestId('DisplayBracket-div'))
        expect(mockFunc).toHaveBeenCalled()
        expect(getByTestId('DisplayBracket-div')).toBeTruthy()
    })
})