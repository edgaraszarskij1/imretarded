import React from 'react';
import MatchForm from '../MatchForm';
import { render, fireEvent } from '@testing-library/react';

describe('MatchForm component', () => {

    const team = {
        team0: 'asd',
        team1: 'gde',
        teamOneScore:0,
        teamTwoScore:0,
    }
    const index = 1
    const event = {}
    const fieldIndex=0

    const onChangeHandler = jest.fn();

    it("Checks if backdrop is loaded", () => {
        const { queryByTestId } = render(
            <MatchForm team={team} />,
        )
        expect(queryByTestId('1')).toBeTruthy()
    })

    it("Checks if dialog is loaded", () => {

        const { queryByTestId } = render(
            <MatchForm team={team} />
        )
        expect(queryByTestId('outter-div-dialog')).toBeTruthy()
    })

    it('Checks if form is loaded', () => {
        const { queryByTestId } = render(
            <MatchForm team={team} />
        )
        expect(queryByTestId('MatchForm-div')).toBeTruthy()
    })

    it('Checks if form shows all required input fields with empty values', () => {
        const { getByTestId } = render(
            <MatchForm team={team}   />
        )
        expect(getByTestId('filter-input-teamOneScore').value).toBe('0')
        expect(getByTestId('filter-input-teamTwoScore').value).toBe('0')
    })

    it('triggers event handler on input change of match score', () => {
        const { getByTestId } = render(
            <MatchForm onChangePointsHandler ={ onChangeHandler} team={team} index = {index} fieldIndex={fieldIndex} />
        )  

        fireEvent.change(getByTestId('filter-input-teamOneScore'),{
            target:{value:'2'}
        })
        fireEvent.change(getByTestId('filter-input-teamTwoScore'),{
            target:{value:'1'}
        })

        expect(onChangeHandler).toHaveBeenCalled();
        expect(getByTestId('filter-input-teamOneScore').value).toBe('2')
        expect(getByTestId('filter-input-teamTwoScore').value).toBe('1')
    })

})