import React from 'react';
import SportsBracket from '../SportsBracket';
import { render, fireEvent} from '@testing-library/react';

describe("SportsBracket component", () => {

    it("shows all required input fields with empty values", () => {
        const { getByTestId } = render(
            <SportsBracket />
        );

        expect(getByTestId("filter-input-name").value).toBe("");
        expect(getByTestId("filter-input-startingDate").value).toBe("");
        expect(getByTestId("filter-input-endingDate").value).toBe("");
        expect(getByTestId("filter-input-teamNumber").value).toBe("");
    });

    it("Checks if component is loaded", () => {
        const { queryByTestId } = render(
            <SportsBracket />,
        )
        expect(queryByTestId('SportsBracket-div')).toBeTruthy()
    })

    it("triggers event handler on input change of tournament name", () => {
        const { getByTestId } = render(
            <SportsBracket />
        );

        fireEvent.change(getByTestId("filter-input-name"), {
            target: { value: "2" },
        });

        fireEvent.mouseDown(getByTestId("filter-input-startingDate"));
        fireEvent.change(getByTestId("filter-input-startingDate"), {
            target:
                { value: "2020-08-24" }
        });

        fireEvent.mouseDown(getByTestId("filter-input-endingDate"));
        fireEvent.change(getByTestId("filter-input-endingDate"), {
            target: { value: "2020-08-24" }
        });

        fireEvent.change(getByTestId("filter-input-teamNumber"), {
            target: { value: "16" },
        });

        expect(getByTestId("filter-input-name").value).toBe("2");
        expect(getByTestId("filter-input-startingDate").value).toBe("2020-08-24");
        expect(getByTestId("filter-input-endingDate").value).toBe("2020-08-24");
        expect(getByTestId("filter-input-teamNumber").value).toBe("16");
    });
})

