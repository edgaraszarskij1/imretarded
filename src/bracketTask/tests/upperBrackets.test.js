const { firstColumnFirstTeamWin } = require('../upperBrackets');

it('Returns object with last match winner assigned to new array of objects with team name filled and position', () => {
    let index = 0;
    let fieldIndex = 0;
    let currRow = 1;
    let currRow2 = 0;
    let column = 1;
    let oldTest = [{
        competingTeamsNumber: "4",
        createdAt: "2020-08-20",
        endingDate: "2020-08-28",
        startingDate: "",
        test: [{
            column: 1,
            date: "",
            row1: 1,
            row2: 0,
            team0: "1",
            team1: "1",
            teamOneScore: '1',
            teamTwoScore: 0,
        }],
        tournamentName: "12",
        tournamentTeams: [{ team0: '1' }, { team1: '1' }]
    }]

    const expectedMatch = [{
        competingTeamsNumber: "4",
        createdAt: "2020-08-20",
        endingDate: "2020-08-28",
        startingDate: "",
        test: [{
            column: 1,
            date: "",
            row1: 1,
            row2: 0,
            team0: "1",
            team1: "1",
            teamOneScore: '1',
            teamTwoScore: 0,

        },
        {
            column: 2,
            currIndex: 1,
            date: "",
            row1: 1,
            row2: 0,
            team0: "1",
            team1: "",
            teamOneScore: 0,
            teamTwoScore: 0,
            win: true
        }],
        tournamentName: "12",
        tournamentTeams: [{ team0: '1' }, { team1: '1' }]
    }]

    const matches = firstColumnFirstTeamWin(index, fieldIndex, currRow, currRow2, column, oldTest, oldTest)
    expect(matches).toEqual(expectedMatch)
})

