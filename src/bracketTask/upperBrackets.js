export const firstColumnFirstTeamWin = (index, fieldIndex, currRow, currRow2, column,oldTest,newState) => {
    oldTest = newState[index].test
    console.log( index, fieldIndex, currRow, currRow2, column,oldTest,newState)
    newState[index] = {
        ...newState[index], test: [...oldTest, {
            team0: newState[index].test[fieldIndex].team0,
            team1: '',
            teamOneScore: 0,
            teamTwoScore: 0,
            date: '',
            currIndex: newState[index].test.length,
            row1: currRow,
            row2: currRow2,
            column: column + 1,
            win: true
        }]
    }
    console.log(newState)
    return newState
}

const firstColumnSecondTeamWin = (index, fieldIndex, currRow, column,oldTest,newState) => {
    oldTest = newState[index].test
    newState[index] = {
        ...newState[index], test: [...oldTest, {
            team0: newState[index].test[fieldIndex].team1,
            team1: '',
            teamOneScore: 0,
            teamTwoScore: 0,
            date: '',
            currIndex: newState[index].test.length,
            row1: currRow,
            column: column + 1,
            win: true,
        }]
    }
}

const secondColumnFirstTeamWin = (index, fieldIndex,currId, currRow2,oldTest,newState) => {
    oldTest = newState[index].test
    newState[index].test[currId] = {
        ...newState[index].test[currId],
        team1: newState[index].test[fieldIndex].team0,
        row2: currRow2,
        win: true
    }
}

const secondColumnSecondTeamWin = (index, fieldIndex,currId, currRow2,oldTest,newState) => {
    oldTest = newState[index].test
    newState[index].test[currId] = {
        ...newState[index].test[currId],
        team1: newState[index].test[fieldIndex].team1,
        row2: currRow2,
        win: true
    }
}


export default {firstColumnFirstTeamWin,firstColumnSecondTeamWin,secondColumnFirstTeamWin,secondColumnSecondTeamWin}