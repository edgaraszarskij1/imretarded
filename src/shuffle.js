function swapIndex(array,currentIndex,nextIndex){
    let temp = null;
    temp = array[currentIndex]
    array[currentIndex] = array[nextIndex]
    array[nextIndex] = temp
}

function shuffle(array){
    let j = 0;
    for(let i = 0; i<array.length; i++){
        j = Math.floor(Math.random()*(i+1))
        swapIndex(array,i,j)
    }
    return array
}

export default shuffle