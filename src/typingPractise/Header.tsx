import React, { useState } from 'react'
import Sidebar from './Sidebar'
import './header.css'



const Header = (props: { children: React.ReactNode }) => {

    const [isOpen, setIsOpen] = useState<boolean>(false)
    const [isClose, setIsClose] = useState<boolean>(false)
    const [clickedOnShadow, setClickedOnShadow] = useState<boolean>(false)

    const openBackdrop = () => {
        setIsOpen(true)
        setIsClose(false)
    }

    const closeBackdrop = (e: React.MouseEvent<HTMLDivElement>) => {
        setIsOpen(false)
        setIsClose(true)
        if ((e.currentTarget).getAttribute('name') === 'shadow') {
            setClickedOnShadow(true)
        } else setClickedOnShadow(false)
    }


    return (
        <div className={'container'}>
            <div className={'text'}>
                STUDYING
        </div>
            <div className={'button-container'}>
                <button className={'button'} >HOME</button>
                <button className={'button'} >ABOUT</button>
                <button className={'button'} >BLOG</button>
                <button className={'button'} >CONTACT</button>
            </div>
            <div className={'sidebar-button-container'} onClick={() => openBackdrop()}>
                <div className={'sidebar-button'}>
                    {isOpen ? <div className={"animated"}></div> : null}
                </div>
            </div>
            <Sidebar isOpen={isOpen} isClose={isClose} shadow={clickedOnShadow} closeBackdrop={closeBackdrop} />
            {props.children}
        </div>
    )
}

export default Header