export interface ResultProperties { 
    timeMS: number, 
    remainingTimeMS: number, 
    word: string, 
    time: string, 
    remainingTime: string 
}