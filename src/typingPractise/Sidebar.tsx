import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import './header.css'

interface Properties {
    isOpen: boolean,
    isClose: boolean,
    shadow: boolean,
    closeBackdrop: (e: React.MouseEvent<HTMLDivElement>) => void
}

const Sidebar = (props: Properties) => {

    const [del, setDel] = useState<boolean>(false)
    const allowedProps: object = { name: 'shadow' }

    useEffect(() => {
        if (props.isOpen) {
            setTimeout(() => {
                setDel(true)
            }, 300)
        }

    }, [props.isOpen])

    useEffect(() => {
        if (!props.isOpen) {
            setTimeout(() => {
                setDel(false)
            }, 300)
        }
    }, [props.isOpen])

    return (
        <>
            {  del ?
                <div className='shadow' {...allowedProps} onClick={(e) => props.closeBackdrop(e)}  >
                    <div className={"sidebar"}>
                        <div className={'sidebar-button-animated'} onClick={(e) => props.closeBackdrop(e)}  >
                            <div className={'sidebar-button-a'}>
                                {!props.isOpen && !props.shadow ? <div className={"animated"}></div> : <div></div>}

                            </div>
                        </div>
                    </div>

                </div> : props.isClose ?
                    <div className={"sidebar-close"}>
                        <div className={'sidebar-button-close'} >
                            <div className={'sidebar-button-a-close'}>
                            </div>
                        </div>
                    </div> : <div></div>}
        </>
    )
}

export default Sidebar;