import React, { useReducer, useState, useEffect, useRef } from 'react';
import './typingPractise.css';
import './button.css';
import FirstRow from './keyboard/FirstRow';
import SecondRow from './keyboard/SecondRow';
import ThirdRow from './keyboard/ThirdRow';
import ForthRow from './keyboard/ForthRow';
import FifthRow from './keyboard/FifthRow';
import { arrayOfWords } from './words/arrayOfWords';
import { randomizeWords } from './words/randomizeWords';
import { timerPerWord } from './timer/timerPerWord';
import Attempts from './display/Attempts';
import Timer from './timer/Timer';
import DisplayResults from './display/DisplayResults';
import { onTyping } from './words/typing';
import CumulativeStatistic from './display/CumulativeStatistic';
import {ResultProperties} from './ResultProperties';

const initialState = {
    input: 0,
    backgroundColor: '',
    keyCode: 0,
    keyLocation: 0,
}

type ACTIONTYPE =
    { type: 'LETTER', value: number } |
    { type: 'PRESSED', keyCode: number, keyLocation: number } |
    { type: 'RELEASED' };

const reducer = (state: typeof initialState, action: ACTIONTYPE) => {
    switch (action.type) {
        case 'LETTER':
            return { ...state, input: state.input + action.value }
        case 'PRESSED':
            return { ...state, backgroundColor: 'gray', keyCode: action.keyCode, keyLocation: action.keyLocation }
        case 'RELEASED':
            return { ...state, backgroundColor: '' }
        default:
            throw new Error()
    }
}

export const TypingPractise = (): JSX.Element => {
    const [state, dispatch] = useReducer(reducer, initialState)
    const [correct, setCorrect] = useState<number>(0)
    const [loadText, setLoadText] = useState<string>(randomizeWords(arrayOfWords))
    const [started, setStarted] = useState<boolean>(false)
    const [stopped, setStopped] = useState<boolean>(false)
    const [position, setPosition] = useState<number>(0)
    const [incorrect, setIncorrect] = useState<number>(100)
    const [total, setTotal] = useState<number>(0)
    const [time, setTime] = useState<number>(timerPerWord(loadText))
    const [result, setResult] = useState<Array<ResultProperties>>([{ timeMS: 0, 
        remainingTimeMS: 0, 
        word: '', time: '', 
        remainingTime: '', }])
    const [successful, setSuccessful] = useState<boolean>(false);
    const [unsuccessful, setUnsuccessful] = useState<boolean>(false)
    const incrementByOne: number = 1
    const keyboardRef = useRef<HTMLInputElement>(null);
    const [isOpen, setIsOpen] = useState(false)

    const handleOpen = () => {
        setIsOpen(true)
    }

    const handleClose = () => {
        setIsOpen(false)
    }

    const handleOnKeyPressed = (event:React.KeyboardEvent<HTMLDivElement>) => {
        dispatch({ type: "LETTER", value: + event.key })
    }

    const handleKeyUp = () => {
        dispatch({ type: "RELEASED" })
    }

    const handleKeyDown = (event:React.KeyboardEvent<HTMLDivElement>) => {
        dispatch({ type: "PRESSED", keyCode: + event.keyCode, keyLocation: event.location })
        lettersChecker(loadText, event.key)
    }

    const startPractise = () => {
        if (!started) {
            setStarted(true)
            setStopped(false)
            setPosition(0)

            if (!unsuccessful) {
                setUnsuccessful(false)
            }
            if (!successful) {
                setSuccessful(false)
            }
            if (keyboardRef && keyboardRef.current) {
                keyboardRef.current.focus();
            }
        } else {
            setTotal(0)
            setCorrect(0)
            setStarted(false)
            setStopped(true)
            setPosition(0)
            setIncorrect(100)
            setTime(timerPerWord(loadText))
            if (!unsuccessful) {
                setUnsuccessful(false)
            }
            if (!successful) {
                setSuccessful(false)
            }
        }
    }

    const newTime = () => {
        return timerPerWord(loadText)
    }

    const lettersChecker = (word: string, letter: string): void => {

        setSuccessful(false)

        if (word[position] === letter) {
            setPosition(prevState => prevState + incrementByOne)
        }

        if (word[position] !== letter) {
            setIncorrect(position)
            setPosition(prevState => prevState)
        }

        if (letter === 'Backspace') {
            setPosition(prevState => prevState - incrementByOne)
            setIncorrect(100)
        }
    }

    const typing = (text: Array<string>, position: number, incorrect: number) => {
        return text.map((element, index) => {
            return <p className={'one-letter'} style={onTyping(index, position, incorrect)}>{element}</p>
        })
    }

    useEffect(() => {
        if (position === loadText.length && loadText.length !== 0 && started) {
            setCorrect(prevState => prevState + incrementByOne)
            setPosition(0)
            setIncorrect(100)
            setSuccessful(true)
            setLoadText(randomizeWords(arrayOfWords))
            setTotal(prevState => prevState + incrementByOne)

        } else setSuccessful(false)
        return () => { }
    }, [loadText, position, started])


    useEffect(() => {
        if (time <= 0 && started) {
            setPosition(0)
            setIncorrect(100)
            setUnsuccessful(true)
            setLoadText(randomizeWords(arrayOfWords))
            setTotal(prevState => prevState + incrementByOne)
        } else setUnsuccessful(false)
        return () => { }
    }, [started, time])

    return (
        <div>
            <div className={'test'}>
                {<Timer
                    started={started} time={time}
                    stopped={stopped}
                    word={loadText}
                    successful={successful}
                    setResult={setResult}
                    newTime={newTime}
                    setTime={setTime}
                    unsuccessful={unsuccessful}
                />}
                <DisplayResults result={result} />
                <button className={'main'} onClick={() => startPractise()}>{!started ? 'Start Practise' : 'Restart Practise'}</button>
                <button className={'cumulative-statistic'} onClick={() => handleOpen()}> Statistics </button>
                <CumulativeStatistic isOpen={isOpen} onClose={handleClose} result={result} />
                <div className={'display'} >
                    {typing(loadText.split(""), position, incorrect)}
                </div>
                <Attempts correct={correct} total={total} />
            </div>
            <div className={'main-container'} tabIndex={0} onKeyPress={handleOnKeyPressed} onKeyDown={handleKeyDown} onKeyUp={handleKeyUp} ref={keyboardRef} >
                <FirstRow keyCode={state.keyCode} backgroundColor={state.backgroundColor} />
                <SecondRow keyCode={state.keyCode} backgroundColor={state.backgroundColor} />
                <ThirdRow keyCode={state.keyCode} keyLocation={state.keyLocation} backgroundColor={state.backgroundColor} />
                <ForthRow keyCode={state.keyCode} keyLocation={state.keyLocation} backgroundColor={state.backgroundColor} />
                <FifthRow keyCode={state.keyCode} keyLocation={state.keyLocation} backgroundColor={state.backgroundColor} />
            </div>
        </div>
    );
};