import React, { useEffect, useState } from 'react'
import '../typingPractise.css'
import './pointers.css'

interface Properties {
    correct: number,
    total: number
}

const Attempts = (props: Properties) => {

    const [oldSuccesuful, setOldSuccesuful] = useState<number>(props.correct)
    const [oldTotal, setOldTotal] = useState<number>(props.total)

    useEffect(() => {
        if (oldSuccesuful < props.correct) {
            setTimeout(() => {
                setOldSuccesuful(props.correct)
            }, 300)
        }
        return () => { }
    }, [props.correct, oldSuccesuful])

    useEffect(() => {
        if (oldSuccesuful !== props.correct && oldTotal < props.total) {
            setTimeout(() => {
                setOldTotal(props.total)
            }, 300)
        }
        return () => { }
    }, [props.correct, oldSuccesuful, oldTotal, props.total])

    return (
        <div className={'attempts-container'}>
            <div className={'attempts-item'}>
                <p className={'text-to-top'}>Successful</p>
                <div className={'text-to-top'}>
                    <div className={'number'}>
                        {oldSuccesuful < props.correct && <div className={'pointer-left'}>{'<'}</div>}
                        {props.correct}
                    </div>
                </div>
            </div>
            <div className={'attempts-item'}>
                <p className={'text-to-top'}>Total</p>
                <div className={'text-to-top'}>
                    <div className={'number'}>
                        {oldSuccesuful === props.correct && oldTotal < props.total && <div className={'pointer-right'}>{'>'}</div>}
                        {props.total}
                    </div>
                </div>
            </div>
        </div>)
}

export default Attempts