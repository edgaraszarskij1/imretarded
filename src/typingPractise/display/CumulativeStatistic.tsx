import React from 'react';
import { startTimer } from '../timer/countdown';
import './cumulativeStatistic.css';
import '../header.css';
import '../button.css';
import {ResultProperties} from '../ResultProperties';

interface Properties {
    result: Array<ResultProperties>,
    onClose: () => void,
    isOpen: boolean
}

const CumulativeStatistic = (props: Properties): JSX.Element => {

    const calculateStats = (): JSX.Element => {

        let totalWords: number = props.result.length,
            totalRemainingTime: number = 0,
            avgRemainingTime: number = 0,
            totalTime: number = 0,
            avgTime: number = 0;

        for (let i = 0; i < props.result.length; i++) {
            totalTime += props.result[i].timeMS
            totalRemainingTime += props.result[i].remainingTimeMS
        }
        avgTime = totalTime / totalWords
        avgRemainingTime = totalRemainingTime / totalWords

        return (
            <>
                <div className={'grid-item-result'}>
                    Average Solved Time: {isNaN(avgRemainingTime) ? '00:00:000' : startTimer(avgRemainingTime)}
                </div>
                <div className={'grid-item-result'}>Total words: {totalWords}</div>
                <div className={'grid-item-result'}>Average Remaining Time: {isNaN(avgTime) ? '00:00:000' : startTimer(avgTime)}</div>
            </>
        )
    }

    return (
        props.isOpen ?
            <div className={'shadow'}>
                <div className={'statistics-container'}>
                    <div className={'grid-box-stat'}>
                        <div className={'grid-item-stat'}>Solved in</div>
                        <div className={'grid-item-stat'}>Word:</div>
                        <div className={'grid-item-stat'}>Remaining time:</div>
                        {props.result.length > 0 &&
                            props.result.map((element) =>
                                <>
                                    <div className={'grid-item-stat'}>{element.remainingTime}</div>
                                    <div className={'grid-item-stat'}>{element.word}</div>
                                    <div className={'grid-item-stat'}>{element.time}</div>
                                </>
                            )
                        }
                        {calculateStats()}

                    </div>
                    <button className={'close-button'} onClick={() => props.onClose()}>Close</button>
                </div>
            </div> :
            <div></div>
    )
}

export default CumulativeStatistic