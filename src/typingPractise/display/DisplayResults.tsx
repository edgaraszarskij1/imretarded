import React from 'react';
import '../typingPractise.css'
import {ResultProperties} from '../ResultProperties';

const DisplayResults = (props: { result: Array<ResultProperties> }): JSX.Element => {

    const everyTenthResult: number = 10

    const result = (element: ResultProperties): string => {
        console.log(element)
        if (element.time === '00:00:000') {
            return 'Solved in: Unsolved ' + element.word + ' Remaining time: ' + element.time
        } else {
            return 'Solved in: ' + element.remainingTime + ' Word: ' + element.word + ' Remaining time: ' + element.time
        }
    }

    return (
        <div className={'display-results'}>
            {props.result.slice(props.result.length - props.result.length - everyTenthResult, props.result.length).map((element) =>
                <p className={'result-items'}>{result(element)}</p>
            )}
        </div>
    )
}

export default DisplayResults