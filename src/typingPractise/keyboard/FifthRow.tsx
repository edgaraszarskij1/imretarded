import React from 'react';

const FifthRow = (props: { keyCode: number, keyLocation: number, backgroundColor: string }) => {
    const { keyCode, keyLocation, backgroundColor } = props
    return (
        <div className={'grid-container'}>
            <p className={'grid-item'} style={keyCode === 17 && keyLocation === 1 ? { backgroundColor: backgroundColor, gridColumn: '1/3', padding: 0 } : { gridColumn: '1/3', padding: 0 }}>CTRL</p>
            <p className={'grid-item'} style={keyCode === 18 && keyLocation === 1 ? { backgroundColor: backgroundColor, gridColumn: '3/5', padding: 0 } : { gridColumn: '3/5', padding: 0 }}>ALT</p>
            <p className={'grid-item'} style={keyCode === 32 ? { backgroundColor: backgroundColor, gridColumn: '5/12', padding: 0 } : { gridColumn: '5/12', padding: 0 }}>SPACE</p>
            <p className={'grid-item'} style={keyCode === 18 && keyLocation === 2 ? { backgroundColor: backgroundColor, gridColumn: '12/14', padding: 0 } : { gridColumn: '12/14', padding: 0 }}>ALT</p>
            <p className={'grid-item'} style={keyCode === 17 && keyLocation === 2 ? { backgroundColor: backgroundColor, gridColumn: '14/16', padding: 0 } : { gridColumn: '14/16', padding: 0 }}>CTRL</p>
            <p className={'grid-item'} style={keyCode === 37 ? { backgroundColor: backgroundColor } : {}}>ARROW LEFT</p>
            <p className={'grid-item'} style={keyCode === 40 ? { backgroundColor: backgroundColor } : {}}>ARROW DOWN</p>
            <p className={'grid-item'} style={keyCode === 39 ? { backgroundColor: backgroundColor } : {}}>ARROW RIGHT</p>
            <p className={'grid-item'} style={keyCode === 96 ? { backgroundColor: backgroundColor, gridColumn: '19/21' } : { gridColumn: '19/21' }}>0</p>
            <p className={'grid-item'} style={keyCode === 110 ? { backgroundColor: backgroundColor } : {}}>.</p>
        </div>
    )
}

export default FifthRow