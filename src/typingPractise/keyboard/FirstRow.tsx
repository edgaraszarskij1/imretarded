import React from 'react'

const FirstRow = (props: { keyCode: number, backgroundColor: string }) => {
    const { keyCode, backgroundColor } = props

    return (
        <div className={'grid-container'} >
            <p className={'grid-item'} style={keyCode === 192 ? { backgroundColor: backgroundColor } : {}}>`</p>
            <p className={'grid-item'} style={keyCode === 49 ? { backgroundColor: backgroundColor } : {}}>1</p>
            <p className={'grid-item'} style={keyCode === 50 ? { backgroundColor: backgroundColor } : {}}>2</p>
            <p className={'grid-item'} style={keyCode === 51 ? { backgroundColor: backgroundColor } : {}}>3</p>
            <p className={'grid-item'} style={keyCode === 52 ? { backgroundColor: backgroundColor } : {}}>4</p>
            <p className={'grid-item'} style={keyCode === 53 ? { backgroundColor: backgroundColor } : {}}>5</p>
            <p className={'grid-item'} style={keyCode === 54 ? { backgroundColor: backgroundColor } : {}}>6</p>
            <p className={'grid-item'} style={keyCode === 55 ? { backgroundColor: backgroundColor } : {}}>7</p>
            <p className={'grid-item'} style={keyCode === 56 ? { backgroundColor: backgroundColor } : {}}>8</p>
            <p className={'grid-item'} style={keyCode === 57 ? { backgroundColor: backgroundColor } : {}}>9</p>
            <p className={'grid-item'} style={keyCode === 48 ? { backgroundColor: backgroundColor } : {}}>0</p>
            <p className={'grid-item'} style={keyCode === 189 ? { backgroundColor: backgroundColor } : {}}>-</p>
            <p className={'grid-item'} style={keyCode === 187 ? { backgroundColor: backgroundColor } : {}}>+</p>
            <p className={'grid-item'} style={keyCode === 8 ? { backgroundColor: backgroundColor, gridColumn: '14/16', padding: 0 } : { gridColumn: '14/16', padding: 0 }}>Backspace</p>
            <p className={'grid-item'} style={keyCode === 45 ? { backgroundColor: backgroundColor } : {}}>INSERT</p>
            <p className={'grid-item'} style={keyCode === 36 ? { backgroundColor: backgroundColor } : {}}>HOME</p>
            <p className={'grid-item'} style={keyCode === 33 ? { backgroundColor: backgroundColor } : {}}>Page Up</p>
            <p className={'grid-item'} style={keyCode === 144 ? { backgroundColor: backgroundColor } : {}}>Num Lock</p>
            <p className={'grid-item'} style={keyCode === 111 ? { backgroundColor: backgroundColor } : {}}>/</p>
            <p className={'grid-item'} style={keyCode === 106 ? { backgroundColor: backgroundColor } : {}}>*</p>
            <p className={'grid-item'} style={keyCode === 109 ? { backgroundColor: backgroundColor } : {}}>-</p>
        </div>
    )
}

export default FirstRow