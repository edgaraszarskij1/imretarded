import React from 'react';

const ForthRow = (props: { keyCode: number, keyLocation: number, backgroundColor: string }) => {
    const { keyCode, keyLocation, backgroundColor } = props
    return (
        <div className={'grid-container'}>
            <p className={'grid-item'} style={keyCode === 16 && keyLocation === 1 ? { backgroundColor: backgroundColor, gridColumn: '1/4', padding: 0 } : { gridColumn: '1/4', padding: 0 }}>Shift</p>
            <p className={'grid-item'} style={keyCode === 90 ? { backgroundColor: backgroundColor } : {}}>Z</p>
            <p className={'grid-item'} style={keyCode === 88 ? { backgroundColor: backgroundColor } : {}}>X</p>
            <p className={'grid-item'} style={keyCode === 67 ? { backgroundColor: backgroundColor } : {}}>C</p>
            <p className={'grid-item'} style={keyCode === 86 ? { backgroundColor: backgroundColor } : {}}>V</p>
            <p className={'grid-item'} style={keyCode === 66 ? { backgroundColor: backgroundColor } : {}}>B</p>
            <p className={'grid-item'} style={keyCode === 78 ? { backgroundColor: backgroundColor } : {}}>N</p>
            <p className={'grid-item'} style={keyCode === 77 ? { backgroundColor: backgroundColor } : {}}>M</p>
            <p className={'grid-item'} style={keyCode === 188 ? { backgroundColor: backgroundColor } : {}}>{'.'}</p>
            <p className={'grid-item'} style={keyCode === 190 ? { backgroundColor: backgroundColor } : {}}>{','}</p>
            <p className={'grid-item'} style={keyCode === 191 ? { backgroundColor: backgroundColor } : {}}>?</p>
            <p className={'grid-item'} style={keyCode === 16 && keyLocation === 2 ? { backgroundColor: backgroundColor, gridColumn: '14/16', padding: 0 } : { gridColumn: '14/16', padding: 0 }}> Shift</p>
            <p className={'grid-item'} style={keyCode === 38 ? { backgroundColor: backgroundColor, gridColumn: '17', padding: 0 } : { gridColumn: '17', padding: 0 }}>ARROW UP</p>
            <p className={'grid-item'} style={keyCode === 97 ? { backgroundColor: backgroundColor, gridColumn: '19' } : { gridColumn: '19' }}>1</p>
            <p className={'grid-item'} style={keyCode === 98 ? { backgroundColor: backgroundColor } : {}}>2</p>
            <p className={'grid-item'} style={keyCode === 99 ? { backgroundColor: backgroundColor } : {}}>3</p>
            <p className={'grid-item'} style={keyCode === 176 && keyLocation === 3 ? { backgroundColor: backgroundColor, height: 83 } : { height: 83 }}> ENTER</p>
        </div>
    )
}

export default ForthRow