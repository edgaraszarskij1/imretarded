import React from 'react';

const SecondRow = (props: { keyCode: number, backgroundColor: string }) => {
    const { keyCode, backgroundColor } = props
    return (
        <div className={'grid-container'}>
            <p className={'grid-item'} style={{ gridColumn: '1/3', padding: 0 }}>Tab</p>
            <p className={'grid-item'} style={keyCode === 81 ? { backgroundColor: backgroundColor } : {}}>Q</p>
            <p className={'grid-item'} style={keyCode === 87 ? { backgroundColor: backgroundColor } : {}}>W</p>
            <p className={'grid-item'} style={keyCode === 69 ? { backgroundColor: backgroundColor } : {}}>E</p>
            <p className={'grid-item'} style={keyCode === 82 ? { backgroundColor: backgroundColor } : {}}>R</p>
            <p className={'grid-item'} style={keyCode === 84 ? { backgroundColor: backgroundColor } : {}}>T</p>
            <p className={'grid-item'} style={keyCode === 89 ? { backgroundColor: backgroundColor } : {}}>Y</p>
            <p className={'grid-item'} style={keyCode === 85 ? { backgroundColor: backgroundColor } : {}}>U</p>
            <p className={'grid-item'} style={keyCode === 73 ? { backgroundColor: backgroundColor } : {}}>I</p>
            <p className={'grid-item'} style={keyCode === 79 ? { backgroundColor: backgroundColor } : {}}>O</p>
            <p className={'grid-item'} style={keyCode === 80 ? { backgroundColor: backgroundColor } : {}}>P</p>
            <p className={'grid-item'} style={keyCode === 219 ? { backgroundColor: backgroundColor } : {}}>{'{'}</p>
            <p className={'grid-item'} style={keyCode === 221 ? { backgroundColor: backgroundColor } : {}}>{'}'}</p>
            <p className={'grid-item'} style={keyCode === 220 ? { backgroundColor: backgroundColor } : {}}>|</p>
            <p className={'grid-item'} style={keyCode === 46 ? { backgroundColor: backgroundColor } : {}}>Delete</p>
            <p className={'grid-item'} style={keyCode === 35 ? { backgroundColor: backgroundColor } : {}}>End</p>
            <p className={'grid-item'} style={keyCode === 34 ? { backgroundColor: backgroundColor } : {}}>Page Down</p>
            <p className={'grid-item'} style={keyCode === 103 ? { backgroundColor: backgroundColor } : {}}>7</p>
            <p className={'grid-item'} style={keyCode === 104 ? { backgroundColor: backgroundColor } : {}}>8</p>
            <p className={'grid-item'} style={keyCode === 105 ? { backgroundColor: backgroundColor } : {}}>9</p>
            <p className={'grid-item'} style={keyCode === 107 ? { backgroundColor: backgroundColor, height: 83 } : { height: 83 }}>+</p>
        </div>
    )
}

export default SecondRow