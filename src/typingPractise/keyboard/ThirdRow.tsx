import React from 'react';

const ThirdRow = (props: { keyCode: number, keyLocation: number, backgroundColor: string }) => {
    const { keyCode, keyLocation, backgroundColor } = props
    return (
        <div className={'grid-container'}>
            <p className={'grid-item'} style={keyCode === 20 ? { backgroundColor: backgroundColor, gridColumn: '1/3', padding: 0 } : { gridColumn: '1/3', padding: 0 }}>Caps Lock</p>
            <p className={'grid-item'} style={keyCode === 65 ? { backgroundColor: backgroundColor } : {}}>A</p>
            <p className={'grid-item'} style={keyCode === 83 ? { backgroundColor: backgroundColor } : {}}>S</p>
            <p className={'grid-item'} style={keyCode === 68 ? { backgroundColor: backgroundColor } : {}}>D</p>
            <p className={'grid-item'} style={keyCode === 70 ? { backgroundColor: backgroundColor } : {}}>F</p>
            <p className={'grid-item'} style={keyCode === 71 ? { backgroundColor: backgroundColor } : {}}>G</p>
            <p className={'grid-item'} style={keyCode === 72 ? { backgroundColor: backgroundColor } : {}}>H</p>
            <p className={'grid-item'} style={keyCode === 74 ? { backgroundColor: backgroundColor } : {}}>J</p>
            <p className={'grid-item'} style={keyCode === 75 ? { backgroundColor: backgroundColor } : {}}>K</p>
            <p className={'grid-item'} style={keyCode === 76 ? { backgroundColor: backgroundColor } : {}}>L</p>
            <p className={'grid-item'} style={keyCode === 186 ? { backgroundColor: backgroundColor } : {}}>:</p>
            <p className={'grid-item'} style={keyCode === 222 ? { backgroundColor: backgroundColor } : {}}>'</p>
            <p className={'grid-item'} style={keyCode === 13 && keyLocation === 0 ? { backgroundColor: backgroundColor, gridColumn: '14/16', padding: 0 } : { gridColumn: '14/16', padding: 0 }}>Enter</p>
            <p></p>
            <p></p>
            <p></p>
            <p className={'grid-item'} style={keyCode === 100 ? { backgroundColor: backgroundColor } : {}}>4</p>
            <p className={'grid-item'} style={keyCode === 101 ? { backgroundColor: backgroundColor } : {}}>5</p>
            <p className={'grid-item'} style={keyCode === 102 ? { backgroundColor: backgroundColor } : {}}>6</p>
        </div>
    )
}

export default ThirdRow