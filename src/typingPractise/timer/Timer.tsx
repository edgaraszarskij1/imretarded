import React, { useState, useEffect, SetStateAction } from 'react';
import { startTimer } from './countdown'
import { timerPerWord } from './timerPerWord';
import {ResultProperties} from '../ResultProperties';

interface Properties {
    started: boolean,
    time: number,
    word: string,
    stopped: boolean,
    successful: boolean,
    unsuccessful: boolean,
    setResult: React.Dispatch<SetStateAction<Array<ResultProperties>>>,
    setTime: React.Dispatch<SetStateAction<number>>,
    newTime: () => number,
}

const Timer = (props: Properties) => {
    let currDate = Date.now()
    const [timeDiff, setTimeDiff] = useState<number>(props.time)
    const [remainingTimeMs, setRemainingTimeMs] = useState<number>(0)
    const [oldWord, setOldWord] = useState<string>(props.word)
    const timeIsOver: number = 0

    useEffect(() => {
        const interval = setInterval(() => {
            if (props.started) {
                props.setTime(prevState => (prevState - (((Date.now() - currDate)))))
                setRemainingTimeMs(timerPerWord(oldWord) - props.time)
            }
        })

        if (props.stopped) {
            setTimeDiff(props.time)
        }

        if (props.time <= 0 && !props.unsuccessful && props.started) {
            props.setResult(prevState =>
                [...prevState, { time: startTimer(timeIsOver)[0], word: oldWord, remainingTime: startTimer(remainingTimeMs)[0], timeMS: timeIsOver, remainingTimeMS: remainingTimeMs }])
            props.setTime(timerPerWord(props.word))
            setTimeDiff(props.time)
            setOldWord(props.word)
        }

        if (props.successful && props.time > 0 && props.started) {
            props.setResult(prevState =>
                [...prevState, { time: startTimer(props.time)[0], word: oldWord, remainingTime: startTimer(remainingTimeMs)[0], timeMS: props.time, remainingTimeMS: remainingTimeMs }])
            props.setTime(props.newTime())
            setOldWord(props.word)
            setTimeDiff(props.time)
        }

        return () => clearInterval(interval);
    },
        [currDate, props.word, props.setResult, oldWord, timeDiff, props, remainingTimeMs])

    return (
        <div className={'timer'}>{props.stopped ? startTimer(remainingTimeMs) : startTimer(props.time)}</div>
    )
}

export default Timer