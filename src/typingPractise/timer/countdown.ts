

export const startTimer = (timeDiff: number): Array<string> => {

    const ms: number = 1000, sec: number = 60
    let minutes: number = 0,
        seconds: number = 0,
        milliseconds: number = 0,
        minDisplay: string | number = 0,
        secDisplay: string | number = 0,
        milisecDisplay: string | number = 0

    minutes = Math.floor(((((timeDiff - milliseconds) / ms) - seconds) / sec) % sec)
    seconds = Math.floor(((timeDiff - milliseconds) / ms) % sec)
    milliseconds = Math.floor(timeDiff % ms)
    minDisplay = minutes < 10 ? "0" + minutes : minutes
    secDisplay = seconds < 10 ? "0" + seconds : seconds
    milisecDisplay = milliseconds < 10 ? "00" + milliseconds : milliseconds

    if (minDisplay && secDisplay && milisecDisplay <= 0) {
        return ['00:00:000']
    }

    return [minDisplay + ':' + secDisplay + ':' + milisecDisplay]
}
