export const timerPerWord = (word: string) => {

    const
        shortLengthWord: number = 3,
        mediumLengthWord: number = 7,
        shortWordTime: number = 1500,
        mediumWordTime: number = 2000,
        longWordTime: number = 3500;

    if (word.length <= shortLengthWord) {
        return shortWordTime
    }
    if (word.length < mediumLengthWord) {
        return mediumWordTime
    }

    return longWordTime

}