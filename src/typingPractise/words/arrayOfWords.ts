export const arrayOfWords: Array<string> = [
    'glittering', 'gem', 'enough',
    'dawned', 'happier', 'herself',
    'abstraction', 'above',
    'surprised', 'immense', 'laziness', 'inspirational',
    'thunderous', 'overhead', 'confirmed'
    , 'toddlers', 'feeding', 'raccoons', 'surprised', 'seasoned',
    'different', 'perception', 'a', 'a', 'a', 'a'
]