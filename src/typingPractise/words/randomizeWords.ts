export const randomizeWords = (array: Array<string>): string => {
    const randomInt: number = Math.floor(Math.random() * Math.floor(array.length))
    return array[randomInt]
}