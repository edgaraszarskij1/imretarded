export const onTyping = (highlightedIndex: number, typedIndex: number, incorrectIndex: number): object => {
    if (highlightedIndex < typedIndex) {
        return { color: 'white' }
    }
    if (highlightedIndex === incorrectIndex) {
        return { color: 'red' }
    }
    if (highlightedIndex === typedIndex) {
        return { color: `skyblue` }
    }

    return { color: 'gray' }

}